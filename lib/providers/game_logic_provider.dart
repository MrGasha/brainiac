import 'dart:async';
import 'dart:math';
import '../widgets/pause_dialog.dart';
import '../screens/sum_game.dart';
import '../screens/home_screen.dart';
import 'package:braniac/widgets/gameover.dart';
import 'package:flutter/material.dart';

import '../models/models.dart';

class GameLogicProvider with ChangeNotifier {
  int level = 1;
  int numbersQuantity = 2;
  final maxNumbersQuantity = 6;
  Score score = Score();
  Duration duration = const Duration(seconds: 30);
  String durationString = '00:30';
  Timer? countDountTimer;
  List<NumberQuestion> numbersQuestions = [];
  List<AnswerModel> answers = [];
  String _gameType = '';
  final _random = Random();
  int min = 10;
  int max = 99;
  int randomNumberGenerator(min, max) => min + _random.nextInt(max - min);
  bool isPlaying = false;
  late BuildContext context;

  void getQuestions() {
    numbersQuestions = [];
    answers = [];
    for (var i = 0; i < numbersQuantity; i++) {
      int number = randomNumberGenerator(min, max);
      numbersQuestions
          .add(NumberQuestion(value: number, valueString: number.toString()));
    }
    answers = getAnswers();
  }

  void initializeGame(String type) {
    level = 1;
    numbersQuantity = 2;
    min = 10;
    max = 99;
    answers = [];
    numbersQuestions = [];
    score.score = 0;
    score.scoreString = score.score.toString();

    if (type == 'Sumas') {
      getQuestions();
    }

    startTimer();

    notifyListeners();
  }

  List<AnswerModel> getAnswers() {
    int answer = 0;
    for (var i = 0; i < numbersQuestions.length; i++) {
      answer += numbersQuestions[i].value;
    }

    final List<AnswerModel> _answers = [];
    final answerList = answer.toString().split('');
    for (var _index = 0; _index < answerList.length; _index++) {
      _answers.add(
        AnswerModel(
          answerList[_index],
          _index,
          TextEditingController(),
          FocusNode(),
        ),
      );
    }
    _answers.last.isEnabled = true;

    _answers.last.focusNode.requestFocus();
    return _answers;
  }

  void updateScore(int _point) {
    score.score += _point;
    score.scoreString = score.score.toString();
    notifyListeners();
  }

  void onSumasPressed() {
    _gameType = 'Sumas';
    initializeGame(_gameType);
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (_c) {
          return const SumGame();
        },
      ),
    );
  }

  void resetFocusedAnswer() {
    answers[answers.length - 1].focusNode.requestFocus();
  }

  void checkAnswer(int index, String userAnswer, _ctx) {
    if (userAnswer == answers[index].value) {
      answers[index].answered = true;
      //if index == 0 is the first digit and first box
      if (index == 0) {
        updateScore(10);
        setCountDown(10);
        levelUp();
        return;
      }
      //if is correct but not index 0 is a regular box
      updateScore(5);
      setCountDown(5);
      nextBox(index);

      return;
    } else {
      penalize(index);
    }
    notifyListeners();
  }

  void penalize(index) {
    updateScore(-5);
    clearCurrentBox(index);
    setCountDown(-5);
  }

  void clearCurrentBox(index) {
    answers[index].focusNode.requestFocus();
    answers[index].answerController.clear();
  }

  void nextBox(index) async {
    answers[index - 1].isEnabled = true;
    answers[index].isEnabled = false;

    await Future.delayed(const Duration(milliseconds: 100))
        .then((value) => answers[index - 1].focusNode.requestFocus());
  }

  void cleanAnswers() {
    answers.map((answer) {
      answer.answerController.clear();
      answer.focusNode.dispose();
      answer.focusNode.unfocus();
    });
  }

  void levelUp() {
    if (numbersQuestions.length < maxNumbersQuantity) {
      min *= 10;
      max = (max * 10.1).toInt();
      numbersQuantity += 1;
    }
    level += 1;
    setCountDown(10);
    cleanAnswers();
    resetFocusedAnswer();
    getQuestions();
  }

  //timers
  void startTimer() {
    countDountTimer = Timer.periodic(const Duration(seconds: 1), (_) {
      setCountDown(-1);
    });
  }

  void stopTimer() {
    countDountTimer!.cancel();
    notifyListeners();
  }

  void resetTimer() {
    stopTimer();
    duration = const Duration(seconds: 30);
  }

  void updateTimer() {
    durationString = duration.toString().substring(2, 7);
  }

  void setCountDown(int _seconds) {
    final seconds = duration.inSeconds + _seconds;
    if (seconds < 0) {
      stopTimer();
      gameOver();
    } else {
      duration = Duration(seconds: seconds);
    }
    updateTimer();
    notifyListeners();
  }

  void gameOver() {
    showGameOver();
  }

  void onRestartPressed() {
    resetTimer();
    cleanAnswers();
    Navigator.of(context).pop();
    initializeGame('Sumas');
  }

  void onContinuePressed() {
    Navigator.of(context).pop();
    startTimer();
  }

  void onMaineMenuPressed() {
    resetTimer();
    cleanAnswers();
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (_c) {
          return const HomeScreen();
        },
      ),
    );
  }

  void showGameOver() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) {
          return WillPopScope(
              child: const GameOver(), onWillPop: () async => false);
        });
  }

  void onMenuPressed(ctx) {
    stopTimer();
    context = ctx;
    showDialog(
        context: context,
        builder: (_) {
          return const PauseDialog();
        });
  }
}
