import 'package:braniac/providers/game_logic_provider.dart';
import 'package:braniac/widgets/crooked_text.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import '../providers/game_logic_provider.dart';
import '../widgets/outlined_styled_button.dart';

class PauseDialog extends StatelessWidget {
  const PauseDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _pauseProvider = Provider.of<GameLogicProvider>(context);
    return Dialog(
      insetAnimationCurve: Curves.decelerate,
      insetAnimationDuration: const Duration(milliseconds: 600),
      backgroundColor: const Color.fromRGBO(57, 91, 100, 1),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30),
      ),
      child: Container(
        width: double.infinity,
        margin: const EdgeInsets.all(15),
        height: MediaQuery.of(context).size.height * 0.4,
        child: Stack(
          alignment: Alignment.center,
          clipBehavior: Clip.none,
          children: <Widget>[
            const Positioned(
              child: CrookedText('Pause'),
              top: -50,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                OutlinedStyledButton(
                  () => _pauseProvider.onRestartPressed(),
                  'Restart',
                ),
                OutlinedStyledButton(
                  () => _pauseProvider.onContinuePressed(),
                  'Continue',
                ),
                OutlinedStyledButton(
                  () => _pauseProvider.onMaineMenuPressed(),
                  'Main Menu',
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
