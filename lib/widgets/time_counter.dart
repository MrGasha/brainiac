import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/game_logic_provider.dart';

class TimeCounter extends StatelessWidget {
  const TimeCounter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final timer = Provider.of<GameLogicProvider>(context);
    timer.context = context;
    return Text(
      timer.durationString,
      style: TextStyle(fontSize: 60, color: Theme.of(context).primaryColor),
    );
  }
}
