import 'package:braniac/providers/game_logic_provider.dart';
import 'package:braniac/widgets/answer.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AnswerList extends StatelessWidget {
  const AnswerList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final answerProvider = Provider.of<GameLogicProvider>(context);
    return SizedBox(
      width: double.infinity,
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ...answerProvider.answers
                  .map(
                    (answer) => Answer(
                        answer.index,
                        answerProvider.checkAnswer,
                        answer.answered,
                        answer.focusNode,
                        answer.answerController,
                        answer.isEnabled),
                  )
                  .toList(),
            ],
          ),
        ],
      ),
    );
  }
}
