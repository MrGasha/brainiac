import 'package:braniac/providers/game_logic_provider.dart';
import 'package:provider/provider.dart';
import 'package:braniac/widgets/crooked_text.dart';
import 'package:braniac/widgets/outlined_styled_button.dart';
import 'package:braniac/widgets/score.dart';
import 'package:flutter/material.dart';

class GameOver extends StatelessWidget {
  const GameOver({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _gameOverProvider = Provider.of<GameLogicProvider>(context);
    return Dialog(
      backgroundColor: const Color.fromRGBO(57, 91, 100, 1),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30),
      ),
      insetAnimationDuration: const Duration(milliseconds: 1000),
      insetAnimationCurve: Curves.bounceOut,
      child: Container(
        padding: const EdgeInsets.all(20),
        margin: const EdgeInsets.all(10),
        height: MediaQuery.of(context).size.height * 0.35,
        width: MediaQuery.of(context).size.width * 0.9,
        child: Stack(
          clipBehavior: Clip.none,
          alignment: Alignment.topCenter,
          children: <Widget>[
            const Positioned(
              child: CrookedText('Game Over'),
              top: -70,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Score(),
                OutlinedStyledButton(
                    () => _gameOverProvider.onRestartPressed(), 'Restart'),
                OutlinedStyledButton(
                    () => _gameOverProvider.onMaineMenuPressed(), 'Main Menu')
              ],
            )
          ],
        ),
      ),
    );
  }
}
