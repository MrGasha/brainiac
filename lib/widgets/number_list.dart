import 'package:provider/provider.dart';
import 'package:braniac/providers/game_logic_provider.dart';
import 'package:braniac/widgets/numbers.dart';
import 'package:flutter/material.dart';

class NumberList extends StatelessWidget {
  const NumberList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final gameLogicProvider = Provider.of<GameLogicProvider>(context);
    return SizedBox(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ...gameLogicProvider.numbersQuestions
              .map(
                (_number) => Numbers(
                  _number.valueString,
                ),
              )
              .toList(),
        ],
      ),
    );
  }
}
