import 'package:flutter/material.dart';
import '../providers/game_logic_provider.dart';
import 'package:provider/provider.dart';

class Score extends StatelessWidget {
  const Score({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final scoreProvider = Provider.of<GameLogicProvider>(context);
    return SizedBox(
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'Score: ' + scoreProvider.score.scoreString,
            style: TextStyle(
              fontSize: 24,
              color: Theme.of(context).primaryColor,
            ),
          ),
          Text(
            'Level: ' + scoreProvider.level.toString(),
            style: TextStyle(
              fontSize: 24,
              color: Theme.of(context).primaryColor,
            ),
          ),
        ],
      ),
    );
  }
}
