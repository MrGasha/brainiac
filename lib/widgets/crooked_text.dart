import 'package:braniac/providers/game_logic_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:math';

class CrookedText extends StatelessWidget {
  final String _text;
  const CrookedText(this._text, {Key? key}) : super(key: key);
  final int _minRotation = -10;
  final int _maxRotation = 10;
  @override
  Widget build(BuildContext context) {
    final crookedTextProvider = Provider.of<GameLogicProvider>(context);
    final List _splittedText = _text.split('');
    return SizedBox(
      child: Row(
        children: <Widget>[
          ..._splittedText
              .map(
                (_char) => Container(
                  margin: const EdgeInsets.symmetric(horizontal: 5),
                  transform: Matrix4.rotationZ(
                    crookedTextProvider.randomNumberGenerator(
                            _minRotation, _maxRotation) *
                        pi /
                        180,
                  ),
                  child: Text(
                    _char,
                    style: TextStyle(
                      color: Theme.of(context).primaryColor,
                    ),
                    textScaleFactor: 3.5,
                  ),
                ),
              )
              .toList()
        ],
      ),
    );
  }
}
