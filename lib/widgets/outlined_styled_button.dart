import 'package:flutter/material.dart';

class OutlinedStyledButton extends StatelessWidget {
  final Function buttonFunction;
  final String buttonText;
  const OutlinedStyledButton(this.buttonFunction, this.buttonText, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      style: ButtonStyle(
        foregroundColor:
            MaterialStateProperty.all(Theme.of(context).primaryColor),
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30),
          ),
        ),
        side: MaterialStateProperty.all(
          BorderSide(color: Theme.of(context).primaryColor),
        ),
        padding: MaterialStateProperty.all(
          const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        ),
      ),
      onPressed: () => buttonFunction(),
      child: Text(
        buttonText,
        style: const TextStyle(
          fontSize: 36,
          letterSpacing: 0.8,
          fontWeight: FontWeight.w400,
        ),
      ),
    );
  }
}
