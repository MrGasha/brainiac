import 'package:flutter/material.dart';
//provider

class Answer extends StatelessWidget {
  final Function checkAnswer;
  final int answerIndex;
  final bool isAnswerd;
  final FocusNode answerFocusNode;
  final TextEditingController anwerController;
  final bool isEnabled;
  const Answer(
    this.answerIndex,
    this.checkAnswer,
    this.isAnswerd,
    this.answerFocusNode,
    this.anwerController,
    this.isEnabled, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 13),
      child: Column(
        children: <Widget>[
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            color: const Color.fromRGBO(57, 91, 100, 0.6),
            child: Container(
              height: MediaQuery.of(context).size.height * 0.08,
              width: MediaQuery.of(context).size.width * 0.12,
              alignment: Alignment.center,
              child: TextField(
                onChanged: (String value) =>
                    checkAnswer(answerIndex, value, context),
                controller: anwerController,
                focusNode: answerFocusNode,
                enabled: isEnabled,
                autofocus: true,
                textAlign: TextAlign.center,
                keyboardType: TextInputType.phone,
                cursorColor: Theme.of(context).primaryColor,
                decoration: const InputDecoration(
                  counterText: "",
                ),
                maxLength: 1,
                style: TextStyle(
                  fontSize: 36,
                  color: Theme.of(context).primaryColor,
                ),
              ),
            ),
          ),
          isAnswerd
              ? Icon(
                  Icons.check,
                  color: Theme.of(context).primaryColor,
                )
              : const SizedBox.shrink(),
        ],
      ),
    );
  }
}
