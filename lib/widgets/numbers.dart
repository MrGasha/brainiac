import 'package:flutter/material.dart';

class Numbers extends StatelessWidget {
  final String numberString;
  const Numbers(this.numberString, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Text(
        numberString,
        style: const TextStyle(
          fontSize: 40,
          letterSpacing: 5,
          color: Color.fromRGBO(57, 91, 100, 1),
        ),
      ),
    );
  }
}
