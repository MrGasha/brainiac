import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/game_logic_provider.dart';

class MenuButton extends StatelessWidget {
  const MenuButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final menuProvider = Provider.of<GameLogicProvider>(context);
    return Container(
      alignment: Alignment.topLeft,
      child: IconButton(
        color: Theme.of(context).primaryColor,
        icon: Icon(
          Icons.pause,
          color: Theme.of(context).primaryColor,
          size: 46,
        ),
        onPressed: () => menuProvider.onMenuPressed(context),
      ),
    );
  }
}
