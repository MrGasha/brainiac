//pkgs
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
//providers
import './providers/game_logic_provider.dart';
// screens
import './screens/home_screen.dart';
import './screens/sum_game.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => GameLogicProvider(),
      child: MaterialApp(
        title: 'Brainiac',
        theme: ThemeData(
          backgroundColor: const Color.fromRGBO(44, 51, 51, 1),
          primaryColor: const Color.fromRGBO(245, 242, 231, 1),
        ),
        home: const HomeScreen(),
      ),
    );
  }
}
