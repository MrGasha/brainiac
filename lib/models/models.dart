import 'package:flutter/material.dart';

class NumberQuestion {
  final int value;
  final String valueString;
  NumberQuestion({
    required this.value,
    required this.valueString,
  });
}

class AnswerModel {
  final String value;
  final int index;
  final FocusNode focusNode;
  final TextEditingController answerController;
  bool answered = false;
  bool isEnabled = false;
  AnswerModel(this.value, this.index, this.answerController, this.focusNode);
}

class Score {
  int score = 0;
  String scoreString = '0';
}
