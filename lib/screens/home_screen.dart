import 'package:braniac/providers/game_logic_provider.dart';
import 'package:braniac/screens/sum_game.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
//widget
import '../widgets/outlined_styled_button.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);
  void _selectSumGame(BuildContext ctx) {
    final _gameLogicProvider =
        Provider.of<GameLogicProvider>(ctx, listen: false);
    _gameLogicProvider.context = ctx;
    _gameLogicProvider.onSumasPressed();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.1,
            width: double.infinity,
          ),
          Text(
            'Seleccione modo de Juego',
            style: TextStyle(
              fontSize: 28,
              color: Theme.of(context).primaryColor,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.35,
          ),
          OutlinedStyledButton(
            () => _selectSumGame(context),
            'Sumas',
          ),
        ],
      ),
    );
  }
}
