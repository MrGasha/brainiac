import 'package:braniac/widgets/answers_list.dart';
import 'package:braniac/widgets/menu_button.dart';
import 'package:braniac/widgets/questions.dart';
import 'package:braniac/widgets/score.dart';
import 'package:braniac/widgets/time_counter.dart';
import 'package:flutter/material.dart';

class SumGame extends StatelessWidget {
  const SumGame({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            children: //aqui va un boton de pause
                <Widget>[
              SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),
              const MenuButton(),
              const TimeCounter(),
              const Score(),
              const Questions(),
              const AnswerList(),

              //score
            ],
          ),
        ),
      ),
    );
  }
}
